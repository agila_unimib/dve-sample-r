---
title: project news
subtitle: project release noted and todo
author: --
date: 2021-10-29
---

# [[project.name]] (development version)

## Issues

* Issue:#22:  `docker` support not yet implemented
* Issue:#22:  `Makefile` support not yet implemented
* Issue:#22:  `Jenkins` support not yet implemented
* Issue:#22:  `renv` support not yet implemented
* Issue:#22:  `drake` support not yet implemented


## Breaking changes

* prev `README.md` moved `notes/usage/README.md`.


## Features and fixes

* `inst/extdata/ext` documented.


# [[project.name]] 0.2.1

* Added `notes` directory.

