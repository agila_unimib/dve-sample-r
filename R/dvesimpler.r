#' @description
#' To learn more about simlab, start with the vignettes:
#' `browseVignettes(package = "dvesimpler")`
#' @keywords internal
"_PACKAGE"

# Suppress R CMD check note
#' @importFrom dplyr tbl
#' @importFrom ggplot2 ggplot
#' @importFrom logging loginfo
#' @importFrom lubridate ymd
#' @importFrom magrittr %>%
#' @importFrom purrr map
#' @importFrom readr read_csv
#' @importFrom stringr str_c
#' @importFrom yaml as.yaml
NULL

